# Add 2 numbers

This project implements a function to add two large numbers represented as strings, similar to how elementary school students perform addition.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

What things you need to install the software and how to install them:

- Node.js
- npm

### Install dependencies:

```
npm install
```

## Running the tests

This project uses [Jest](https://jestjs.io/) for testing. To run the tests, use the following command:

```
npm run test
```

## Built With

- [Node.js](https://nodejs.org/)
- [Winston](https://github.com/winstonjs/winston) - A logger for just about everything.

## File structure

```
    .
    ├─── logs
    ├─── node_modules
    ├─── src
    │    ├─── logger
    │    └─── MyBigNumber.js   # Algorithm
    ├─── tests
    ├─── .gitignore
    ├─── package-lock.json
    ├─── package.json
    └─── README.md
```

## Authors

- Do Anh Khoa - Backend Developer - [KhoaDoesTech](https://github.com/KhoaDoesTech)
