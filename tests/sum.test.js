const MyBigNumber = require('../src/MyBigNumber');

describe('Add2Num', () => {
  it('Adding two positive numbers', async () => {
    const result = MyBigNumber.sum('200', '200');
    expect(result).toBe('400');
  });
  it('Adding a positive number and zero', async () => {
    const result = MyBigNumber.sum('200', '0');
    expect(result).toBe('200');
  });
  it('Adding two large positive numbers', async () => {
    const result = MyBigNumber.sum('999999999999999999999999999999', '1');
    expect(result).toBe('1000000000000000000000000000000');
  });
});

