const logger = require('./logger/config.logger');

class MyBigNumber {
  sum(stn1, stn2) {
    logger.info(`Adding ${stn1} and ${stn1}`);

    stn1 = stn1.split('').reverse().join('');
    stn2 = stn2.split('').reverse().join('');

    let result = '';
    let carry = 0;
    let maxLength = Math.max(stn1.length, stn2.length);

    for (let i = 0; i < maxLength; i++) {
      let digit1 = i < stn1.length ? parseInt(stn1[i]) : 0;
      let digit2 = i < stn2.length ? parseInt(stn2[i]) : 0;
      let sum = digit1 + digit2 + carry;
      result += sum % 10;
      carry = Math.floor(sum / 10);

      // Logging the operation
      let logMessage = `Step ${i}: ${digit1} + ${digit2} + carry (${carry}) = ${sum}, write down ${
        sum % 10
      }, next carry = ${Math.floor(sum / 10)}`;
      logger.info(logMessage);
    }

    if (carry !== 0) {
      result += carry;
    }

    result = result.split('').reverse().join('');

    logger.info(`Result::: ${result}`);
    return result;
  }
}

// const myBigNumber = new MyBigNumber();
// const result = myBigNumber.sum('123', '456');

// console.log(result);

module.exports = new MyBigNumber();
